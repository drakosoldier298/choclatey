# Choclatey 1.19.2
Created by Daedreus#3431 on discord. [VOIDCREW](https://discord.gg/gRmAas4Pgk)
# The modlist update work for v0.2.0 started v0.2.0 oct/28/2022
## So what is this
- This is a reimagining of [vanillia](https://gitlab.com/Merith-TK/vanillia-modpack) A modpack with optimization and quality of life in mind, This modpack is the same idea, However My ideals for that modpack eventually differed from the owner, so I left the project and with permission made this!

## What is the idea of this pack
- Didn't you write this in "so what is this" Yes and no.  
So our main focus is actually quality of life without performance loss. So adding things like, Journeymap, JEI(fabric equivilent), Shaders, Hud improvements. Stuff like that, This modpack is also focused on being ALL SERVER COMPATIBLE Mainly using hypixel and mineplex, and other clients like badlion and lunar as guidlines, If they dont allow it neither will I.

## How do i install this?  
 -  For now you can only install it with [PrismLauncher](https://prismlauncher.org/). Or [Multimc](https://multimc.org/) This is because it runs on [Packwiz](https://packwiz.infra.link/) Which allows me to automatically update mods remotely, meaning no need for you to ever reinstall a mod or check for mod updates yourself! There is currently no way to install it on Curseforge launcher,ATLauncher, or the default launcher, ***Support for the default launcher is coming soon! <sup>tm</sup>***

## There will be no forge version for a **long** while
- The reasoning for this is forge for clientside modding isnt the best, its clunky and not super optimizable, optifine cant even help anymore and hasn't since 1.13, I do know how to make a forge version of this pack but due to how well optimized fabric is by default its easier to just focus on fabric

## Wheres the modlist?!?!
- This will be added overtime, it takes alot of time to write these out as i have to manually grab the links code them into hyperlinks and a bunch of other jargon that at 11pm I'd rather not deal with. For now to check what mods are installed you can either, check the files, or download it yourself.


## Are you aware the pack is mispelled?
- Yes! that is actually intential, as vanillia was mispelled, i would like to continue that joke!

## Will there be an x version of this pack
- Depends on how much effort it is to make, As of writing my main focus is 1.19.2 and 1.18.2 anything older is possible to support but unlikely, anything newer is a definite yes.


# Modlist
## **[No Chat Reports](https://modrinth.com/mod/no-chat-reports)**
 - This disables that pesky chat report feature that mojang added, When somone reports you it will send a blank player id, making them unable to identify you.

 # Optional mods
   [Simple voice chat]()
## Performance (optifine replacements)
[Iris](https://modrinth.com/mod/iris), [Sodium](https://modrinth.com/mod/sodium), [Starlight](https://modrinth.com/mod/starlight), [LazyDfu](https://modrinth.com/mod/lazydfu), [Indium](https://modrinth.com/mod/indium), [Sodium Extra](https://modrinth.com/mod/sodium-extra) These mods replace optifine and help improve performance father than optifine ever did.



[Cull Less Leaves](https://modrinth.com/mod/cull-less-leaves)
 - Instead of leaving just the outer layer of leaves, Cull Less Leaves also renders a certain amount of layers defined in the config. Making you able to have "fast leaf performance" while not having trees look weird.

 [Smooth Boot](https://modrinth.com/mod/smoothboot-fabric)
  - speeds up loading the game.

[CTM Refabricated](https://modrinth.com/mod/ctm-refabricated)
- I don't feel the need to explain this one, it adds connected textures to the game.

[Entity Texture Features](https://modrinth.com/mod/entitytexturefeatures)
 - Add's the ability for texture packs to add variation to entities.
 
## Essential mods
[Modmenu](https://modrinth.com/mod/modmenu) 
 - This mod allows you to edit mod configs from ingame, as well as see descriptions of mods installed.

[ViaFabric](https://modrinth.com/mod/viafabric)
 - allows connecting to other versions of minecraft!

[LambDynamicLights](https://modrinth.com/mod/lambdynamiclights)
 - adds dynamic lights, when you hold a torch it will light up the area around you! This does not only deal with torches, other glowing items will also emit light!

[ESSENTIAL Mod](https://modrinth.com/mod/essential) 
 - This mod adds alot, mainly, Friends list, direct connecting to friends, connecting to friends singleplayer worlds, cosmetics.
 - Mostly bedrocklike features.


## Quality of life.
[Armour Statues](https://modrinth.com/mod/armor-statues/version/v4.0.2-1.19.2-Fabric)
 - This mod allows you to customize armour stands much much nicer, According to the mod dev it should work on any server! 

[Better Statistics Screen](https://modrinth.com/mod/better-stats)
 - changes the statistics screen to look a bit nicer
 - It looks alot like iris/sodiums menu looks.

[Continuity](https://modrinth.com/mod/continuity)
 - Technically would go with optifine replacements, This mod adds small amounts of connected textures.

[Not enough crashes](https://modrinth.com/mod/notenoughcrashes)
 - Instead of crashing to desktop this mod pulls you to the mainmenu and tells you what **it thinks** went wrong!

[Betterf3](https://modrinth.com/mod/betterf3) 
 - This mod colour codes, and allows you to customize your f3 menu!

[Animatica](https://modrinth.com/mod/animatica)
- Another mod that technically belongs in optifine replacements, It allows using animated texture format, so most texture packs that say "optifine only" that deal with item textures work without optifine!

[Visuality](https://modrinth.com/mod/visuality)
- Adds a few new visuals, like when you hit a skeleton bone particles come off, when a slime jumps theres slimey particles underneath it!

[MoreChatHistory](https://modrinth.com/mod/morechathistory)
 - I dont think i need to explain this one, It allows chat history to be longer.


[Vanilla Notebook](https://modrinth.com/mod/notebook)
- adds a notebook to your game so you can take notes ingame without needing a book and quill.

[Recipe Book is Pain](https://modrinth.com/mod/rbip),
[Better Recipe Book](https://www.curseforge.com/minecraft/mc-mods/brb)
 - These 2 mods work together to make the recipe book a bit nicer to use. adding better grouping, more recipe lookup's like furnace recipes and even brewing recipes.

[ItemSwapper](https://modrinth.com/mod/itemswapper)
 - This mod adds a very beta idea to allow swapping between multiple item types with a button hit.

[Dynamic Crosshair](https://modrinth.com/mod/dynamiccrosshair)
[Dynamic Crosshair Compat](https://modrinth.com/mod/dynamiccrosshaircompat)
- these 2 mods work together to make a dynamic crosshair that changes when different things happen, say you arent looking at anything, it will hide the crosshair.

[JourneyMap](https://modrinth.com/mod/journeymap)
 -  a mini and fullmap mod!It allows you set on screen waypoints, automatically creates death waypoints so you know where you died.
 - THIS MOD IS OPTIONAL, you choose it on startup!

[Chat Coords](https://modrinth.com/mod/chat-coords)
 - Allows you to send coordinates to chat with a press of a button!

[No Strip](https://modrinth.com/mod/no-strip)
 - Allows you to add a keybind to stop you from accidently stripping logs. 
## Misc
a collection of mods i added because i thought people would like them

[Would You Shut Up, Man?](https://modrinth.com/mod/would-you-shut-up-man)
 - allows you to mute somone clientside.

[Item despawn flicker](https://modrinth.com/mod/item-despawn-flicker)
 - Makes items flicker when they are about to despawn

[ExtraSounds](https://modrinth.com/mod/extrasounds)
[bad packets](https://modrinth.com/mod/badpackets)
 - Helpfull for debugging problems.

[Lighty](https://modrinth.com/mod/lighty)
 - A light overlay mod, allows you to see where those pesky mobs can spawn.

 [Distinguished Potions](https://modrinth.com/mod/distinguished-potions)
  - Are you tired of not being able to tell what potion is what without hovering over them? This mod makes all potions have a distinct colour!

  [SomeOrdinaryTweaks](https://modrinth.com/mod/ordinarytweaks)
   - SomeOrdinaryTweaks is a mod that adds few GUI/HUD/Rendering Tweaks and gives ability to turn off selective toast groups (tutorial/advancement/recipe unlock popups), also includes few misc features. every feature is listed in the link.

[KronHUD](https://modrinth.com/mod/kronhud)
 - A mod that allows editing a lot of your hud and add new features to the hud. 

[Colour My Servers](https://modrinth.com/mod/cms)
 - allows you to colour your servers and make the server list unique.
 <sub/>in all honesty i have no idea why i added this i guess i thought it was cool</sub>

[Recursive Resources](https://modrinth.com/mod/recursiveresources)
 - Allows you to organze resource packs by adding folders! you can enable entire folders at once.
 
[ Playlist](https://modrinth.com/mod/playlist)
 - adds a playlist function for all minecraft music, meaning you can choose what songs can play!

[thorium](https://modrinth.com/mod/thorium)
 - fixes a ton of small litlle minecraft bugs.

[Screenshot to Clipboard](https://modrinth.com/mod/screenshot-to-clipboard)
 -  sends your recent screenshot to clipboard so you can paste them immediatly.

[Battery Status Info](https://modrinth.com/mod/batterystatusinfo)
 - Adds a battery status for the laptop players!
- Note this is broken for desktop users and will always show as 0%

[EMICompat](https://modrinth.com/mod/emicompat) 
 - allows emi to be compatible with more things, not usefull too much for non modded instances, but i added it anyway.

 [BactroMod](https://modrinth.com/mod/bactromod)
 - adds quite a few random tweaks, Best to check the mod description.

 [Embedded Assets](https://modrinth.com/mod/embedded_assets)
  - Datapacks will now download with the required resource pack as one item instead of multiple!

## Beautify mods.
[Dynamic Music Updated](https://modrinth.com/mod/dynamic-music-updated)
 - adds dynamic music.
[Emissive Skin Renderer](https://modrinth.com/mod/emissive-skin-renderer)
 - allows you to add glowing bits to your player model!

[Celestria](https://modrinth.com/mod/celestria)
 - adds shooting stars to your skybox! Thats..thats it

[Better Command Block UI ](https://modrinth.com/mod/bettercommandblockui)
 - It really is just a better command block ui.

[NiceLoad](https://modrinth.com/mod/niceload)
 - A fancier loading screen mod for Fabric. This displays the current process of (re)loading game resources.

[Falling Leaves](https://modrinth.com/mod/fallingleaves)
 - adds falling leaf particles under leaf blocks!


## Ui improvements
[Visible Toggle Sprint](https://modrinth.com/mod/visible-toggle-sprint)
 - allows you to see when your toggle sprint is active.

[Unsuspicious Stew](https://modrinth.com/mod/unsuspicious-stew)
 - allows you to see what effect a suspicious stew will give you

[Nether Coords](https://modrinth.com/mod/nether-coords)
 - adds a clientside command for calculating nether coordinates

[Peek](https://modrinth.com/mod/peek)
 - allows you to "peek" inside things without placing them, like shulker boxes.



[Smooth Swapping](https://modrinth.com/mod/smooth-swapping)
 - adds an animation when you quick swap items between chests.

[UI Input Undo](https://modrinth.com/mod/ui-input-undo-fabric)
 - Allows you to undo and redo things with inputs like text box typing.

[Inspecio](https://modrinth.com/mod/inspecio) 
 - tired of having to place down and open a shulker box to see inside it? Now you dont have to!, just hover over it!

[Mouse Wheelie](https://modrinth.com/mod/mouse-wheelie)
 - A small mod to enable various mouse wheel related actions. Features item scrolling, inventory sorting, item refilling and much more!

[MidnightControls](https://modrinth.com/mod/midnightcontrols)
 - allows players to use a controller.

[AdvancedChatBox](https://modrinth.com/mod/advancedchatbox),[AdvancedChatCore](https://modrinth.com/mod/advancedchatcore),[AdvancedChatHUD](https://modrinth.com/mod/advancedchathud)
 - these three mods make chatbox a little nicer, allowing you to resize it and make multiple tabs at will.
 - Requires you to make one tab upon joining your first world/server

[Raised](https://modrinth.com/mod/raised)
- raises the hot bar a few pixels so it cant get cut off

[Eating Animation](https://modrinth.com/mod/eating-animation)
- adds an eating animation.

[EMI](https://modrinth.com/mod/emi),[EMI Trades](https://modrinth.com/mod/emitrades)
 - EMI is just another version of jei, its better used in vanilla environments as modded scenerios do not fully support it, 
 - allows you to easily look up recipe's and view the recipe tree, so what it takes to full make x easily!

[Grid](https://modrinth.com/mod/grid)
 - Allows you to enable a grid and even change how many blocks between eachmarker, usefull for building and not counting everything. 

[Stendhal](https://modrinth.com/mod/stendhal)
 - adds more to signs and book writing! like emoji's and the ability to copy paste!

## Library mods
[DarkKore](https://modrinth.com/mod/darkkore)
- Library for better chat function. and Kronhud

[Cloth Config API](https://modrinth.com/mod/cloth-config)
 - almost every mod uses this.

[Fabric API](https://modrinth.com/mod/fabric-api)
 - Every mod uses this